#include "SoundEmitterVolume.h"

#include <Components/SplineComponent.h>
#include <Components/SplineMeshComponent.h>
#include <Components/AudioComponent.h>
#include <Components/SphereComponent.h>
#include <Components/BillboardComponent.h>
#include <Components/ArrowComponent.h>
#include <ProceduralMeshComponent.h>
#include <KismetProceduralMeshLibrary.h>
#include <Engine/StaticMesh.h>

ASoundEmitterVolume::ASoundEmitterVolume()
	: Super()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bAllowTickOnDedicatedServer = false;
	PrimaryActorTick.TickInterval = 0.1f;

	bIsClockWiseLoop = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	RootComponent = SphereComponent;
	SphereComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	SplineComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	SplineComponent->AddRelativeLocation(FVector(0.f, 0.f, 5.f));//add Z offset
	SplineComponent->SetClosedLoop(true);

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	AudioComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	AudioComponent->SetAutoActivate(false);
#if WITH_EDITOR
	SoundEmitterSprite = CreateDefaultSubobject<UBillboardComponent>(TEXT("SoundEmitterSprite"));
	SoundEmitterSprite->AttachToComponent(AudioComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SoundEmitterSprite->SetSprite(LoadObject<UTexture2D>(nullptr, TEXT("/Engine/EditorResources/AudioIcons/S_AudioComponent.S_AudioComponent")));

	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialRef(TEXT("Material'/SoundTools/M_DebugWall.M_DebugWall'"));
	InsidePlaneMaterial = MaterialRef.Object;

	InsidePlane = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("DebugWall"));
#endif
}

void ASoundEmitterVolume::BeginPlay()
{
	Super::BeginPlay();

	if (GetNetMode() == ENetMode::NM_DedicatedServer)
		return;

	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnSphereComponentBeginOverlap);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &ThisClass::OnSphereComponentEndOverlap);

	TSet<AActor*> Actors;
	GetOverlappingActors(Actors, TSubclassOf<APawn>());
	for (auto Actor : Actors)
	{
		auto Pawn = Cast<APawn>(Actor);
		if (Pawn && Pawn->IsLocallyControlled())
		{
			Target = Pawn;
			UpdateSoundEmitterLocation(Target);
			AudioComponent->Play();
		}
	}
}

void ASoundEmitterVolume::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GetWorld()->WorldType != EWorldType::Editor && GetWorld()->WorldType != EWorldType::EditorPreview && Target)
		UpdateSoundEmitterLocation(Target);
}

void ASoundEmitterVolume::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (GetNetMode() != ENetMode::NM_DedicatedServer)
	{
		SphereComponent->OnComponentBeginOverlap.RemoveDynamic(this, &ThisClass::OnSphereComponentBeginOverlap);
		SphereComponent->OnComponentEndOverlap.RemoveDynamic(this, &ThisClass::OnSphereComponentEndOverlap);
	}


	Super::EndPlay(EndPlayReason);
}

bool ASoundEmitterVolume::ShouldTickIfViewportsOnly() const
{
	return true;
}

void ASoundEmitterVolume::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);

#if WITH_EDITOR
	UpdateZoneInfo();
#endif
}

#if WITH_EDITOR
void ASoundEmitterVolume::UpdateZoneInfo()
{
	if (bDrawZoneInfo)
	{
		TArray<FVector> Vertices;
		int32 i = 0;
		for (i; i < SplineComponent->GetNumberOfSplinePoints(); i++)
		{
			auto PointLocation = SplineComponent->GetLocationAtSplinePoint(i, ESplineCoordinateSpace::World);
			auto PointRightVector = SplineComponent->GetRightVectorAtSplinePoint(i, ESplineCoordinateSpace::World);
			auto InsideRotation = bIsClockWiseLoop ? PointRightVector.Rotation() : (-PointRightVector).Rotation();
			UArrowComponent* Arrow = nullptr;

			if (InsidePlane)
				Vertices.Add(PointLocation);

			if (i >= ZoneArrows.Num())
			{
				Arrow = NewObject<UArrowComponent>(this, { TEXT("Arrow%d"), i });
				if (!Arrow)
					continue;
				Arrow->RegisterComponent();
				Arrow->AttachToComponent(SplineComponent, FAttachmentTransformRules::KeepRelativeTransform);
				ZoneArrows.Add(Arrow);
			}
			else
			{
				Arrow = ZoneArrows[FSetElementId::FromInteger(i)];
			}

			if (Arrow)
			{
				Arrow->SetWorldLocation(PointLocation);
				Arrow->SetWorldRotation(InsideRotation);
			}
		}

		for (i; i < ZoneArrows.Num(); i++)
		{
			ZoneArrows[FSetElementId::FromInteger(i)]->DestroyComponent();
			ZoneArrows.Remove(FSetElementId::FromInteger(i));
		}

		if (InsidePlane)
		{
			TArray<int32> Triangles;
			int32 HeadIndex = 0;
			int32 TailIndex = SplineComponent->GetNumberOfSplinePoints() - 1;
			while (TailIndex > HeadIndex)
			{
				Triangles.Add(TailIndex);
				Triangles.Add(HeadIndex);
				Triangles.Add(HeadIndex + 1);
	
				if (TailIndex - HeadIndex > 2)//for second tris wee need minimum 4 points
				{
					Triangles.Add(TailIndex);
					Triangles.Add(HeadIndex + 1);
					Triangles.Add(TailIndex - 1);
				}
				//Move Head and Tail to new position
				++HeadIndex;
				--TailIndex;
			}
	
			TArray<FVector> Normals;
			TArray<FProcMeshTangent> Tangents;
	
			UKismetProceduralMeshLibrary::CalculateTangentsForMesh(Vertices, Triangles, TArray<FVector2D>(), Normals, Tangents);
			
			InsidePlane->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, TArray<FVector2D>(), TArray<FLinearColor>(), Tangents, false);
			InsidePlane->SetMaterial(0, InsidePlaneMaterial);
		}
	}
	else
	{
		for (auto ArrowItr = ZoneArrows.CreateIterator(); ArrowItr; ++ArrowItr)
		{
			(*ArrowItr)->DestroyComponent();
			ArrowItr.RemoveCurrent();
		}

		if (InsidePlane)
		{
			if (InsidePlane->GetNumSections())
			{
				InsidePlane->ClearAllMeshSections();
			}
		}
	}
}
#endif

void ASoundEmitterVolume::UpdateSoundEmitterLocation(APawn* Pawn)
{
	FVector TargetLocation = Target->GetActorLocation();
	float Param = SplineComponent->FindInputKeyClosestToWorldLocation(TargetLocation);

	FVector NearestSplinePoint = SplineComponent->GetTransformAtSplineInputKey(Param, ESplineCoordinateSpace::World).GetLocation();
	FVector RightVector = SplineComponent->GetRightVectorAtSplineInputKey(Param, ESplineCoordinateSpace::World);
	//if loop is clockwise right vector oriented inside loop
	FVector ComparedVector = bIsClockWiseLoop ? RightVector : RightVector * (-1.f);

	FVector SoundEmitterLocation = FVector::DotProduct(ComparedVector, NearestSplinePoint - TargetLocation) < 0 ? TargetLocation : NearestSplinePoint;

	AudioComponent->SetWorldLocation(SoundEmitterLocation);

#if WITH_EDITOR
	static const auto CVarDebugMode = IConsoleManager::Get().FindConsoleVariable(TEXT("st.DebugMode"));
	if (CVarDebugMode->GetInt() != (!SoundEmitterSprite->bHiddenInGame))
		SoundEmitterSprite->SetHiddenInGame(CVarDebugMode->GetInt());
#endif
}

void ASoundEmitterVolume::OnSphereComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto Pawn = Cast<APawn>(OtherActor);
	if (Pawn && Pawn->IsLocallyControlled())
	{
		Target = Pawn;
		UpdateSoundEmitterLocation(Target);
		AudioComponent->Play();
	}
}

void ASoundEmitterVolume::OnSphereComponentEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && OtherActor == Target)
	{
		Target = nullptr;
		AudioComponent->Stop();
	}
}