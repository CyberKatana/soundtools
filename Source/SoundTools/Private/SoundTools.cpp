#include "SoundTools.h"

#define LOCTEXT_NAMESPACE "FSoundToolsModule"

static TAutoConsoleVariable<int32> CVarDebugMode(
	TEXT("st.DebugMode"),
	0,
	TEXT("DebugMode"),
	ECVF_SetByConsole
);

void FSoundToolsModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
}

void FSoundToolsModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FSoundToolsModule, SoundTools)