#pragma once

#include <GameFramework/Actor.h>

#include "SoundEmitterVolume.generated.h"

class USplineComponent;
class UAudioComponent;
class USphereComponent;

UCLASS(ClassGroup=("SoundTools"))
class SOUNDTOOLS_API ASoundEmitterVolume : public AActor
{
	GENERATED_BODY()
public:

	ASoundEmitterVolume();

	//~ Begin AActor Interface
	void BeginPlay() override;
	void Tick(float DeltaSeconds) override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	bool ShouldTickIfViewportsOnly() const override;
	virtual void OnConstruction(const FTransform& Transform) override;
	//~ End AActor Interface

protected:

	UPROPERTY(EditAnywhere)
	class USplineComponent* SplineComponent;

	UPROPERTY()
	class UProceduralMeshComponent* InsidePlane;

	UPROPERTY(EditAnywhere)
	class UAudioComponent* AudioComponent;

	UPROPERTY(EditAnywhere)
	class USphereComponent* SphereComponent;

	UPROPERTY(EditAnywhere)
	class UStaticMesh* StaticMesh;

	UPROPERTY(EditAnywhere)
	bool bIsClockWiseLoop;

#if WITH_EDITORONLY_DATA

	class UBillboardComponent* SoundEmitterSprite;

	//~Begin UObject interface
 	//~End UObject interface

	UPROPERTY(EditInstanceOnly)
	bool bDebugMode;
	
	UPROPERTY(EditInstanceOnly)
	bool bDrawZoneInfo;

	class UMaterial* InsidePlaneMaterial;

	TSet<class UArrowComponent*> ZoneArrows;

	void UpdateZoneInfo();

#endif

	APawn* Target;

	void UpdateSoundEmitterLocation(class APawn* Pawn);

	UFUNCTION()
	void OnSphereComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnSphereComponentEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};